import message from "./message";

let element;

document.addEventListener("DOMContentLoaded", () => {
    element = document.createElement("h1");
    element.textContent = message;
    document.body.appendChild(element);
});

if (module.hot) {
    module.hot.accept("./message", () => {
        // Hot update the <h1> element if the message changes
        if (element) {
            element.textContent = message;
        }
    });
}
