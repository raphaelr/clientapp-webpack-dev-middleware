# clientapp-webpack-dev-middleware

This project demonstrates how to use a plain webpack app (as opposed to a create-react-app/Angular CLI app) in ASP.NET Core.

Here you can see how to do this using `WebpackDevMiddleware`, which is **OBSOLETE**. This repository exists soley to compare the new way (over at https://gitlab.com/raphaelr/clientapp-spaservices-extensions) with the old way.

For more details, see https://tapesoftware.net/aspnet-clientapp/.